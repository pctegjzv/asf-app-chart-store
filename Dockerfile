FROM alpine:3.7

RUN mkdir -p /asf-chart-store

COPY . /asf-chart-store

RUN chmod +x /asf-chart-store/app.sh && \
    mv /asf-chart-store/yq /usr/local/bin

CMD ["/asf-chart-store/app.sh"]