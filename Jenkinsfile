// https://jenkins.io/doc/book/pipeline/syntax/
@Library('alauda-cicd') _

def GIT_BRANCH
def GIT_COMMIT
def IMAGE
def DEBUG = false
def RELEASE_VERSION
def RELEASE_BUILD

pipeline {
    agent {
        node 'all'
    }

    options {
        // 保留多少流水线记录（建议不放在jenkinsfile里面）
        buildDiscarder(logRotator(numToKeepStr: '10'))

        // 不允许并行执行
        disableConcurrentBuilds()
    }

    environment {
      TAG_CREDENTIALS = "alaudabot-bitbucket"
      DEPLOYMENT = "asf-app-chart-store"
      DINGDING_BOT = "asf-robot"
      OWNER = "mathildetech"
      REPOSITORY = "asf-app-chart-store"
    }

    stages {
      stage('Checkout') {
        steps {
          script {
              // checkout code
              def scmVars = checkout scm
              env.GIT_COMMIT = scmVars.GIT_COMMIT
              env.GIT_BRANCH = scmVars.GIT_BRANCH
              GIT_COMMIT = "${scmVars.GIT_COMMIT}"  // xyz
              GIT_BRANCH = "${scmVars.GIT_BRANCH}"  // fix/one-bug
              RELEASE_VERSION = readFile('.version').trim()  // v0.1
              RELEASE_BUILD = "${RELEASE_VERSION}.${env.BUILD_NUMBER}"  // v0.1.1
              if (GIT_BRANCH != "master") {
                  def branch = GIT_BRANCH.replace("/","-").replace("_","-")  // fix-one-bug
                  RELEASE_BUILD = "${RELEASE_VERSION}.${branch}.${env.BUILD_NUMBER}"  //v0.1.fix-one-bug.1
              }
          }
          // installing golang coverage and report tools
          sh """
              go get -u github.com/alauda/gitversion
          """
          sh "go get -u github.com/mikefarah/yq"
          script {
              if (GIT_BRANCH == "master") {
                  sh "gitversion patch ${RELEASE_VERSION} > patch"
                  RELEASE_BUILD = readFile("patch").trim()
              }
              echo "release ${RELEASE_VERSION} - release build ${RELEASE_BUILD}"
          }
        }
      }
      stage('CI'){
        failFast true
        parallel {
          stage('Build') {
              steps {
                  sh 'cp $(which yq) .'
                  script {
                    IMAGE = deploy.dockerBuild(
                        "Dockerfile", //Dockerfile
                        ".", // build context
                        "index.alauda.cn/alaudak8s/asf-chart-store", // repo address
                        "${RELEASE_BUILD}", // tag
                        "alaudak8s", // credentials for pushing
                    )
                    // start and push
                    IMAGE.start().push().push(GIT_COMMIT)
                  }
              }
          }
        }
      }
      stage('Promoting') {
          // limit this stage to master only
          when {
              expression {
                  GIT_BRANCH == "master"
              }
          }
          steps {
              script {
                // promote to release
                IMAGE.push("release")

                // adding tag to the current commit
                withCredentials([usernamePassword(credentialsId: TAG_CREDENTIALS, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                    sh "git tag -l | xargs git tag -d" // clean local tags
                    sh """
                        git config --global user.email "alaudabot@alauda.io"
                        git config --global user.name "Alauda Bot"
                    """
                    def repo = "https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/${OWNER}/${REPOSITORY}.git"
                    sh "git fetch --tags ${repo}" // retrieve all tags
                    sh("git tag -a ${RELEASE_BUILD} -m 'auto add release tag by jenkins'")
                    sh("git push ${repo} --tags")
                }

                build job: '../../charts-pipeline', parameters: [
                  [$class: 'StringParameterValue', name: 'CHART', value: 'devops'],
                  [$class: 'StringParameterValue', name: 'VERSION', value: RELEASE_VERSION],
                  [$class: 'StringParameterValue', name: 'COMPONENT', value: 'asf-chartstore'],
                  [$class: 'StringParameterValue', name: 'IMAGE_TAG', value: RELEASE_BUILD],
                  [$class: 'BooleanParameterValue', name: 'DEBUG', value: false],
                  [$class: 'StringParameterValue', name: 'ENV', value: ''],
                ], wait: false
              }
          }
      }      
    }

    post {
        // 成功
        success {
          echo "Horay!"
          script {
            deploy.notificationSuccess("${DEPLOYMENT}", DINGDING_BOT, "流水线完成了", "${GIT_BRANCH} ${GIT_COMMIT} ${RELEASE_VERSION}")
          }
        }
        // 失败
        failure {
            echo "damn!"
            script {
                deploy.notificationFailed("${DEPLOYMENT}", DINGDING_BOT, "流水线失败了", "${GIT_BRANCH} ${GIT_COMMIT} ${RELEASE_VERSION}")
            }
        }
        // 取消的
        aborted {
          echo "aborted!"
        }
    }
}
