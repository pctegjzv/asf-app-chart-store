OS = Linux

test:
	helm lint config-server && helm lint eureka && helm lint hadoop && helm lint hbase && helm lint hystrix-dashboard && helm lint kafka && helm lint turbine && helm lint zookeeper && helm lint zuul

build:
	docker build . -t index.alauda.cn/alaudak8s/asf-chart-store:latest

push:
	docker push index.alauda.cn/alaudak8s/asf-chart-store:latest

preview:
	helm install --debug --dry-run ./python

clear:
	kubectl delete chart $(kubectl get chart  --all-namespaces |awk '{print $1}')