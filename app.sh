#!/usr/bin/env sh
SHOULD_CHANGE_IMAGE=""
DEFAULT_REGISTRY="index.alauda.cn"
DEFAULT_REGISTRY_NAMESPACE="alaudak8s"
SHOULD_CHANGE_CODE=""
DEFAULT_GIT="https://github.com"
DEFAULT_GIT_NAMESPACE="alauda-asf-quickstarts"
if [ "$REGISTRY" != "" ]; then
    echo "Chaging registry address to $REGISTRY"
    DEFAULT_REGISTRY=$REGISTRY
    SHOULD_CHANGE_IMAGE="1"
fi;
if [ "$REGISTRY_NAMESPACE" != "" ]; then
    echo "Chaging registry namespace to $REGISTRY_NAMESPACE"
    DEFAULT_REGISTRY_NAMESPACE=$REGISTRY_NAMESPACE
    SHOULD_CHANGE_IMAGE="1"
fi;

if [ "$SHOULD_CHANGE_IMAGE" != "" ]; then
    echo "Applying new registry configuration $DEFAULT_REGISTRY/$DEFAULT_REGISTRY_NAMESPACE..." 
    yq w -i /asf-chart-store/config-server/values.yaml registryAddress "$DEFAULT_REGISTRY/$DEFAULT_REGISTRY_NAMESPACE"
    yq w -i /asf-chart-store/eureka/values.yaml registryAddress "$DEFAULT_REGISTRY/$DEFAULT_REGISTRY_NAMESPACE"
    yq w -i /asf-chart-store/hadoop/values.yaml registryAddress 
    "$DEFAULT_REGISTRY/$DEFAULT_REGISTRY_NAMESPACE"
    yq w -i /asf-chart-store/hbase/values.yaml registryAddress "$DEFAULT_REGISTRY/$DEFAULT_REGISTRY_NAMESPACE"
    yq w -i /asf-chart-store/hystrix-dashboard/values.yaml registryAddress "$DEFAULT_REGISTRY/$DEFAULT_REGISTRY_NAMESPACE"
    yq w -i /asf-chart-store/kafka/values.yaml registryAddress "$DEFAULT_REGISTRY/$DEFAULT_REGISTRY_NAMESPACE"
    yq w -i /asf-chart-store/turbine/values.yaml registryAddress "$DEFAULT_REGISTRY/$DEFAULT_REGISTRY_NAMESPACE"
    yq w -i /asf-chart-store/zookeeper/values.yaml registryAddress "$DEFAULT_REGISTRY/$DEFAULT_REGISTRY_NAMESPACE"
    yq w -i /asf-chart-store/zuul/values.yaml registryAddress "$DEFAULT_REGISTRY/$DEFAULT_REGISTRY_NAMESPACE"
fi
cp -r /asf-chart-store /opt/