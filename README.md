# asf-app-chart-store

用于存储 `asf` 中生成应用的模板，当基于此模板生成一应用，会创建一个 `release`，这个 `release` 包含了 `templates` 目录下定义的资源。

如果某个模板内容有修改，请修改 `Chart.meta.yaml` 中的 `labels --> alauda.io/product.version` 的值，例如从 `0.1.0` 修改为 `0.1.1`。